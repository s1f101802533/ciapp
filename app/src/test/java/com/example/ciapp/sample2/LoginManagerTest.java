package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;
    private LoginManager loginManager2;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("TestUser1", "Pass1234");

    }

    @Test
    public void testLoginSuccess() throws LoginFailedException, InvalidPasswordException,UserNotFoundException {

        User user = loginManager.login("TestUser1", "Pass1234");
        assertThat(user.getUsername(), is("TestUser1"));
        assertThat(user.getPassword(), is("Pass1234"));
    }

    @Test(expected = UserNotFoundException.class)
    public void testUserNotFound() throws LoginFailedException, InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("iniad", "password");   
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws LoginFailedException, InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("TestUser1", "P1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Iniad", "Pass1234");
    }

}
